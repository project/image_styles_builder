# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- add official support of drupal 10.4
- add official support of drupal 11.1

## [1.1.2] - 2024-08-20
### Removed
- remove legacy version annotation on docker-compose.yml

### Changed
- upgrade Docker Database mariadb 10.3.8 -> 10.6

### Fixed
- fix obsolete docker-compose command

### Added
- add official support of drupal 11.0

## [1.1.1] - 2024-05-31
### Added
- add coverage of Drupal 10.1.x
- add coverage of Drupal 10.2.x
- add coverage of Drupal 10.3.x
- add Drupal GitlabCI
- add cspell project words for Gitlab-CI

### Removed
- drop tests support on Drupal <= 9.4
- remove mglaman/drupal-check tool

### Fixed
- fix deprecated usage of ContainerAwareTrait use DI instead

## [1.1.0] - 2023-01-27
### Added
- add changelog format in order to use Keep a Changelog standard
- add dependabot for Github Action dependency
- add drupal-check check
- add support Drupal 9.5
- add upgrade-status check
- add official support of drupal 9.5 & 10.0

### Fixed
- fixed docker test on CI race-condition database note ready

### Removed
- drop support of drupal below 9.3.x

## [1.0.0] - 2022-06-24
### Added
- Init module

[Unreleased]: https://github.com/antistatique/drupal-image-styles-builder/compare/1.1.2...HEAD
[1.1.2]: https://github.com/antistatique/drupal-image-styles-builder/compare/1.1.1...1.1.2
[1.1.1]: https://github.com/antistatique/drupal-image-styles-builder/compare/1.1.0...1.1.1
[1.1.0]: https://github.com/antistatique/drupal-image-styles-builder/compare/1.0.0...1.1.0
[1.0.0]: https://github.com/antistatique/drupal-image-styles-builder/releases/tag/1.0.0
