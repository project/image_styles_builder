# Developing on Image Styles Builder

* Issues should be filed at
https://www.drupal.org/project/issues/image_styles_builder
* Pull requests can be made against
https://github.com/antistatique/drupal-image-styles-builder/pulls

## 📦 Repositories

Drupal repo

  ```bash
  git remote add drupal \
  git@git.drupal.org:project/image_styles_builder.git
  ```

Github repo

  ```bash
  git remote add github \
  git@github.com:antistatique/drupal-image-styles-builder.git
  ```

## 🔧 Prerequisites

First of all, you need to have the following tools installed globally
on your environment:

  * drush
  * The latest dev release of Image Styles Builder
  * docker
  * docker compose

### Project bootstrap

Once run, you will be able to access to your fresh installed Drupal on `localhost::8888`.

    docker compose build --pull --build-arg BASE_IMAGE_TAG=10.3 drupal
    # (get a coffee, this will take some time...)
    docker compose up --build -d drupal
    docker compose exec -u www-data drupal drush site-install standard --db-url="mysql://drupal:drupal@db/drupal" -y

    # You may be interested in resetting the admin password of your Docker.
    docker compose exec drupal drush user:password admin admin

    # Enable the module to use it.
    docker compose exec drupal drush en image_styles_builder

## 🏆 Tests

We use the [Docker for Drupal Contrib images](https://hub.docker.com/r/wengerk/drupal-for-contrib) to run testing on our project.

Run testing by stopping at first failure using the following command:

    docker compose exec -u www-data drupal phpunit --group=image_styles_builder --no-coverage --stop-on-failure --configuration=/var/www/html/phpunit.xml

## 🚔 Check Drupal coding standards & Drupal best practices

During Docker build, the following Static Analyzers will be installed on the Docker `drupal` via Composer:

- `drupal/coder^8.3.1`  (including `squizlabs/php_codesniffer` & `phpstan/phpstan`),

The following Analyzer will be downloaded & installed as PHAR:

- `phpmd/phpmd`
- `sebastian/phpcpd`
- `wapmorgan/PhpDeprecationDetector`
- `vimeo/psalm`

### Command Line Usage

    ./scripts/hooks/post-commit
    # or run command on the container itself
    docker compose exec drupal bash

#### Running Code Sniffer Drupal & DrupalPractice

https://github.com/squizlabs/PHP_CodeSniffer

PHP_CodeSniffer is a set of two PHP scripts; the main `phpcs` script that tokenizes PHP, JavaScript and CSS files to
detect violations of a defined coding standard, and a second `phpcbf` script to automatically correct coding standard
violations.
PHP_CodeSniffer is an essential development tool that ensures your code remains clean and consistent.

  ```
  $ docker compose exec drupal phpcs /var/www/html/modules/contrib/image_styles_builder/
  ```

Automatically fix coding standards

  ```
  $ docker compose exec drupal phpcbf /var/www/html/modules/contrib/image_styles_builder/
  ```

#### Running PHP Mess Detector

https://github.com/phpmd/phpmd

Detect overcomplicated expressions & Unused parameters, methods, properties.

  ```
  $ docker compose exec drupal phpmd /var/www/html/modules/contrib/image_styles_builder text ./phpmd.xml \
  --suffixes php,module,inc,install,test,profile,theme,css,info,txt --exclude *Test.php,*vendor/*
  ```

  ```
  $ docker compose exec drupal phpmd text ./phpmd.xml \
  --suffixes php,module,inc,install,test,profile,theme,css,info,txt --exclude *Test.php,*vendor/*
  ```

  ```
  $ docker compose exec drupal phpmd text ./phpmd.xml -suffixes php
  ```

#### Running PHP Copy/Paste Detector

https://github.com/sebastianbergmann/phpcpd

`phpcpd` is a Copy/Paste Detector (CPD) for PHP code.

  ```
  $ docker compose exec drupal phpcpd /var/www/html/modules/contrib/image_styles_builder \
--names=*.php,*.module,*.inc,*.install,*.test,*.profile,*.theme,*.css,*.info,*.txt --names-exclude=*.md,*.info.yml \
--exclude tests --exclude vendor/ --ansi
  ```

#### Running PhpDeprecationDetector

https://github.com/wapmorgan/PhpDeprecationDetector

A scanner that checks compatibility of your code with PHP interpreter versions.

  ```
  $ docker compose exec drupal phpdd /var/www/html/modules/contrib/image_styles_builder \
    --file-extensions php,module,inc,install,test,profile,theme,info --exclude vendor
  ```

### Enforce code standards with git hooks

Maintaining code quality by adding the custom post-commit hook to yours.

  ```bash
  cat ./scripts/hooks/post-commit >> ./.git/hooks/post-commit
  ```
